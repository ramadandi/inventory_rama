<?php

include ('koneksi.php');


if(!isset($_SESSION)){
	session_start();
}

if(isset($_POST['username'])){
	$username = mysqli_real_escape_string($koneksi, addslashes(trim($_POST['username'])));
	$password = mysqli_real_escape_string($koneksi,trim($_POST['password']));

	$query = mysqli_query($koneksi, "SELECT * FROM petugas WHERE username = '$username' AND password = '$password'");
	$data = mysqli_fetch_array($query);
	$cek=mysqli_num_rows($query);
	if ($cek>0)
	{
		$_SESSION['username'] = $username;
		$_SESSION['id_level'] =  $data['id_level'];
		header("location:successlogin.php");
	}
}
	
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Edmin</title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
    </head>
<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.php">
				Login Inventaris Sekolah				
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					
					
					<li class="">						
						<a href="../login.php" class="">
							<i class="icon-chevron-left"></i>
							Login sebagai Member 
						</a>
						
					</li>
					<li class="">						
						<a href="../pegawai/login.php" class="">
							<i class="icon-chevron-left"></i>
							Login sebagai Pegawai 
						</a>
						
					</li>
				</ul>
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->

	<br>
	<div class="span7">
		
		<form action="" method="post">
		
			<h1>Login Admin</h1>		
			
			<div class="login-fields">
				
                <p>Tolong masukan detail akun anda</p>
				
				<div class="field">
					<label for="username">username</label>
					<input type="text" pattern="[A-Za-z]{8,0}" id="username" autocomplete="off" name="username" value="" placeholder="username" maxlength="20" class="login username-field" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" value="" placeholder="Password" maxlength="20" class="login password-field"/>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
									
                <button class="button btn btn-success btn-large">Sign In</button>
				<?php
				if (isset($cek) && (!$cek))
				{
					echo 'Login gagal, username atau password salah<br><br>';
				}
				?>

               
				
			</div> <!-- .actions -->
			
			
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->





<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>

<script src="js/signin.js"></script>

</body>

</html>
