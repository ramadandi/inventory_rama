<?php
include ('cek.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminLTE 2 | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue sidebar-mini">
      
      <header class="main-header">
        <!-- Logo -->
        <a class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Inventaris</b></span>
        </a>
        <ul class="nav pull-right">
                        
    </ul>
        <!-- Header Navbar: style can be found in header.less -->

              <!-- Control Sidebar Toggle Button -->
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>Menu admin</p>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
          
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="inventaris.php"><i class="fa fa-circle-o"></i> Inventaris</a></li>
              </ul>
            </li>
 <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="peminjaman.php"><i class="fa fa-circle-o"></i> Peminjaman</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="pengembalian.php"><i class="fa fa-circle-o"></i> Pengembalian</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="ruang.php"><i class="fa fa-circle-o"></i> Ruang</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="pegawai.php"><i class="fa fa-circle-o"></i> Pegawai</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="jenis.php"><i class="fa fa-circle-o"></i> Jenis</a></li>
              </ul>
            </li>
            <br>
            <br>
            <br>
            <br>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="logout.php"><i class="fa fa-circle-o"></i> Logout</a></li>
              </ul>
            </li>
          </ul>
        </section>
      </aside>
      <div class="row">
            <div class="col-xs-9">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Table Inventaris</h3>
                </div><!-- /.box-header -->
                <p>
                                    <a href="tambah_pegawai.php" class="btn btn-primary fa fa-plus">Tambah pegawai</a>
                  
                                </p>
                                
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Id Pegawai</th>
                        <th>Nama pegawai</th>
                        <th>Nip</th>
                        <th>Alamat</th>
                            <th>Aksi</th>
                      </tr>
                    </thead>
                    <?php
                                include "koneksi.php";
                                $no=1;
                                $select=mysqli_query($koneksi,"select * from pegawai");
                                while($data=mysqli_fetch_array($select))
                                {
                                ?>
                                  <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['nama_pegawai']; ?></td>
                                    <td><?php echo $data['nip']; ?></td>
                                    <td><?php echo $data['alamat']; ?></td>
                                
                                
                            
                                    
                                    
                                    <td><a class="btn btn outline btn-primary fa fa-edit" href="edit_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>">Edit</a> 
                                <td>
                                        <a class="btn btn outline btn-danger fa fa-trash-o" href="hapus_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>">Hapus</a>
                                        </td>
                                    
                                     
                                        

                                    </tr>
                                    <?php
                                }
                                ?>
                                        
                                    </tbody>                
                  </table>
          </div>
        </div>
      </div>
    </div>
  </body>
  </html>

            
    <!-- jQuery 2.1.3 -->
    <script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- jQuery UI 1.11.2 -->
    <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <!-- Morris.js charts -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/knob/jquery.knob.js" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js" type="text/javascript"></script>    
    
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js" type="text/javascript"></script>    
    
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js" type="text/javascript"></script>
  </body>
</html>