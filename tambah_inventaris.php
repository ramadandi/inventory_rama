<?php include('koneksi.php') ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminLTE 2 | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
                    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Inventaris
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="simpan_inventaris.php" method="post">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Id Inventaris</label>
                       <input  class="form-control" >
                      
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama</label>
                      <input required=""  class="form-control" name="nama">
                    </div>
                      <div class="form-group">
                      <label for="exampleInputPassword1">Kondisi</label>
                      <input  required="" class="form-control" name="kondisi" >
                    </div>
                      <div class="form-group">
                      <label for="exampleInputPassword1">Keterangan</label>
                      <input  class="form-control" name="keterangan" >
                    </div>
                      <div class="form-group">
                       <label for="exampleInputPassword1">Jumlah</label>
                      <input  class="form-control" name="jumlah" >
                    </div>
                      <div class="form-group">
                      <label for="exampleInputPassword1">Id Jenis</label>
                      <?php $jenis = mysqli_query($koneksi,"SELECT * FROM jenis");
                                            
                                            ?>
                                            <select name="id_jenis" class="form-control">
                                              <?php foreach ($jenis as $a): ?>
                                              <option value="<?php echo $a['id_jenis'] ?>" ><?php echo $a['nama_jenis'] ?></option>                                                
                                              <?php endforeach ?>
                                            </select> 
                    </div>
                      <div class="form-group">
                      <label for="exampleInputPassword1">Id Ruang</label>
                      <?php $ruang = mysqli_query($koneksi,"SELECT * FROM ruang");
                                            
                                            ?>
                                            <select name="id_ruang" class="form-control">
                                              <?php foreach ($ruang as $b): ?>
                                              <option value="<?php echo $b['id_ruang'] ?>" ><?php echo $b['nama_ruang'] ?></option>                                                
                                              <?php endforeach ?>
                                            </select> 
                    </div>
                      <div class="form-group">
                      <label for="exampleInputPassword1">Kode Inventaris</label>
                      <input  class="form-control" name="kode_inventaris" >
                    </div>
                      <div class="form-group">
                      <label for="exampleInputPassword1">Id Petugas</label>
                      <?php $petugas = mysqli_query($koneksi,"SELECT * FROM petugas");
                                            
                                            ?>
                                            <select name="id_petugas" class="form-control">
                                              <?php foreach ($petugas as $c): ?>
                                              <option value="<?php echo $c['id_petugas'] ?>" ><?php echo $c['nama_petugas'] ?></option>                                                
                                              <?php endforeach ?>
                                            </select> 
                    </div>
                  </div>
                    <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>

                                        
                 
			 </select>
                                            </div>
                                        </div>
                                      
                    <?php
                                             include "koneksi.php";
                  $select = mysqli_query($koneksi,"SELECT * FROM inventaris");
                  while($data = mysqli_fetch_array($select))
                  {
                    ?>
                    <center>
                      
                    </center>
                    <?php } ?>
			 </select>
                                  

						
			   
                   <script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="scripts/common.js" type="text/javascript"></script>
      
    </body>
