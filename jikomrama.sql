-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2019 at 05:27 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jikomrama`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `kondisi` varchar(25) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `jumlah` float NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`) VALUES
(13, 'Maradita Apriani', 'Bagus', 'Baik', 2, 2, '2019-01-15 13:27:14', 1, 'ABC-0002', 2);

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(25) NOT NULL,
  `kode_jenis` varchar(11) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'Laptop', 'ABC-0001', 'Fullset'),
(2, 'Infocus', 'ABC-0002', 'Baik');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'Administrator'),
(2, 'Operator'),
(3, 'Peminjam');

-- --------------------------------------------------------

--
-- Table structure for table `login_pegawai`
--

CREATE TABLE `login_pegawai` (
  `id` int(11) NOT NULL,
  `nip` int(15) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `jabatan` varchar(15) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(20) NOT NULL,
  `no_hp` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_pegawai`
--

INSERT INTO `login_pegawai` (`id`, `nip`, `nama`, `jabatan`, `username`, `password`, `no_hp`) VALUES
(1, 1223265776, 'kurdi', 'Guru', 'kurdi', 'kurdi123', '08734634224');

-- --------------------------------------------------------

--
-- Table structure for table `login_user`
--

CREATE TABLE `login_user` (
  `id` int(11) NOT NULL,
  `nis` int(15) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `kelas` varchar(15) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(20) NOT NULL,
  `no_hp` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_user`
--

INSERT INTO `login_user` (`id`, `nis`, `nama`, `kelas`, `username`, `password`, `no_hp`) VALUES
(1, 1374328423, 'ipung', '12rpl2', 'ipung', 'ipung123', '089662323434');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(25) NOT NULL,
  `nip` int(11) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(1, 'Alief Juan Aprian', 1121323234, 'Jl.Veteran gg Kepatihan 04/01'),
(2, 'Reza', 2233434, 'Laldon'),
(3, 'rama', 1223344, 'laladon ');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(15) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`) VALUES
(1, '2019-01-15 09:00:00', '2019-01-15 11:40:00', 'Sudah Kembali', 1),
(2, '2019-01-16 11:48:30', '2019-01-16 11:48:30', 'jkjk', 1);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_petugas` varchar(35) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `id_level`) VALUES
(1, 'administrator', '12345', 'Alief', 1),
(2, 'operator', 'kenapa123', 'Juan', 2),
(3, 'peminjam', 'apasilo', 'Aprian', 3);

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(30) NOT NULL,
  `kode_ruang` varchar(11) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(1, 'Lab-1', 'R-01', 'R-Laptop');

-- --------------------------------------------------------

--
-- Table structure for table `view`
--

CREATE TABLE `view` (
  `id` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(110) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `view`
--

INSERT INTO `view` (`id`, `deskripsi`, `gambar`) VALUES
(2, '<p>SMK Negeri 1 Ciomas mulai beroperasional pada Tahun Pelajaran 2008/2009. Menginjak usianya yang ketiga pada Tahun Pelajaran 2011/2012, SMK Negeri 1 Ciomas telah membuka 2(dua) bidang keahlian dan 3(tiga) bidang keahlian yaitu Bidang Keahlian Teknik Otomotif program keahlian Teknik Kendaraan Ringan (TKR), serta Bidang Keahlian Teknik Inforamasi dan Komunikasi dengan Program Keahlian Rekayasa Perangkat Lunak (RPL) dan Animasi. Lalu pada tahun pelajaran 2015/2016 dibuat jurusan baru yaitu Teknik Pengelasan(TPL).</p>\r\n\r\n<p>Luas area yang telah disetujui Kepala Dinas Pendidikan Kabupaten Bogor seluruhnya kurang lebih 6.500 meter kubik. Area ini berbentuk persegi, namun ada bagian tanah masyarakat yang masih menjorok ke bagian dalam lokasi sekolah (seluas 745 meter kubik). Bagian utara, barat dan selatan berbatasan langsung dengan tanah masyarakat, sementara bagian timur yang merupakan bagian depan SMK menghadap ke jalan Raya Laladon.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Alamat</td>\r\n			<td>&nbsp;&nbsp;&nbsp;:</td>\r\n			<td>&nbsp;&nbsp;&nbsp;Jl. Desa Laladon No.21 Kecamatan Ciomas Kabupaten Bogor</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Telepon</td>\r\n			<td>&nbsp;&nbsp;&nbsp;:</td>\r\n			<td>&nbsp;&nbsp;&nbsp;(0251) 7520933</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Web</td>\r\n			<td>&nbsp;&nbsp;&nbsp;:</td>\r\n			<td>&nbsp;&nbsp;&nbsp;smkn1ciomas.com</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Email</td>\r\n			<td>&nbsp;&nbsp;&nbsp;:</td>\r\n			<td>&nbsp;&nbsp;&nbsp;smkn1_ciomas@yahoo.co.id</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Kode Pos</td>\r\n			<td>&nbsp;&nbsp;&nbsp;:</td>\r\n			<td>&nbsp;&nbsp;&nbsp;16610</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '1.jpg'),
(4, '<p>Rekayasa Perangkat Lunak Di indonesia dijadikan disiplin ilmu yang dipelajari mulai tingkat Sekolah Menengah Kejuruan sampai tingkatan Perguruan Tinggi. Di tingkat SMK, jurusan ini sudah memiliki kurikulum materi pelajaran sendiri yang sudah ditentukan oleh Dinas Pendidikan.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p>Saat Ini Jurusan RPL adalah jurusan yang paling banyak muridnya dari pada jurusan lain di SMK Negeri 1 Ciomas. Termasuk kami, kami di SMKN 1 Ciomas adalah jurusan RPL.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p>Kepala Program Jurusan Saat ini adalah Pak Hasrul Adipura Harahap S.Kom, Yang mengajar tentang bahasa pemrograman C++ dan Java Script. Guru mapel Produktif RPL Lainnya ialah Pak Heru Setiawan S.Kom, yang mengajar tentang web dinamis dan juga database mysql. Ada guru lainnya seperti Pak Avian Sofiansyah,Pa Gatot Imam S.Kom, Pak Erwan Usmawan S.Kom.</p>\r\n', '2.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
